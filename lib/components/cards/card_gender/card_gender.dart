import 'package:flutter/material.dart';

import '../../../constants.dart';

const double iconSize = 80.0;
const double sizedBoxHeight = 15.0;

class GenderCardTemplate extends StatelessWidget {
  final IconData icon;
  final String genderLabel;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(icon, size: iconSize),
        const SizedBox(height: sizedBoxHeight),
        Text(
          genderLabel,
          style: kLabelTextStyle,
        ),
      ],
    );
  }

  const GenderCardTemplate(
      {Key? key, required this.icon, required this.genderLabel})
      : super(key: key);
}
