import 'package:flutter/material.dart';

import '../../constants.dart';

class BottomButton extends StatelessWidget {
  final String buttonText;
  final VoidCallback handleOnTap;

  const BottomButton({
    required this.buttonText,
    required this.handleOnTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: handleOnTap,
      child: Container(
        padding: const EdgeInsets.only(bottom: 10.0),
        alignment: Alignment.center,
        color: kBottomContainerColor,
        height: kBottomContainerHeight,
        width: double.infinity,
        // margin: const EdgeInsets.only(top: 10.0),
        child: Text(
          buttonText,
          style: kLargeButtonStyle,
        ),
      ),
    );
  }
}
