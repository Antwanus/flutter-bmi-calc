import 'dart:math';

class BmiCalculatorService {
  final int heightInCm;
  final int weightInKg;
  late double _bmi;

  BmiCalculatorService({
    required this.heightInCm,
    required this.weightInKg,
  }) {
    _bmi = (weightInKg / pow(heightInCm / 100, 2));
  }

  String getBmiAsString() {
    return _bmi.toStringAsFixed(1);
  }

  String getBmiResultText() {
    if (_bmi >= 25) {
      return "Overweight";
    } else if (_bmi > 18.5) {
      return "Normal";
    } else {
      return "Underweight";
    }
  }

  String getBmiResultInterpretation() {
    if (_bmi >= 25) {
      return "Big boi! lorem lorem lorem lorem lorem lorem lorem lorem lorem l";
    } else if (_bmi > 18.5) {
      return "Méh! lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem";
    } else {
      return "Underweight! lorem lorem lorem lorem lorem lorem lorem lorem lor";
    }
  }
}
