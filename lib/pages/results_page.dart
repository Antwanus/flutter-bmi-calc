import 'package:flutter/material.dart';
import 'package:flutter_bmi_calculator/components/cards/card_base.dart';
import 'package:flutter_bmi_calculator/constants.dart';

import '../components/buttons/button_footer.dart';

class ResultPage extends StatelessWidget {
  final String bmiResult;
  final String bmiResultText;
  final String bmiResultInterpretation;

  const ResultPage({
    Key? key,
    required this.bmiResult,
    required this.bmiResultText,
    required this.bmiResultInterpretation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("BMI Calculator"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(10.0),
              alignment: Alignment.center,
              child: const Text("Your Result", style: kTitleTextStyle),
            ),
          ),
          Expanded(
            flex: 6,
            child: BaseCardTemplate(
              color: kActiveCardColor,
              cardChild: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(bmiResultText.toUpperCase(), style: kResultTextStyle),
                  Text(bmiResult, style: kBMIResultTextStyle),
                  Text(
                    bmiResultInterpretation,
                    textAlign: TextAlign.center,
                    style: kResultBodyTextStyle,
                  ),
                ],
              ),
            ),
          ),
          BottomButton(
            buttonText: "RE-CALCULATE",
            handleOnTap: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
