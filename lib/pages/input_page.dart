import 'package:flutter/material.dart';
import 'package:flutter_bmi_calculator/components/buttons/button_footer.dart';
import 'package:flutter_bmi_calculator/pages/results_page.dart';
import 'package:flutter_bmi_calculator/services/bmi_calculator_service.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../components/buttons/button_round_icon.dart';
import '../components/cards/card_base.dart';
import '../components/cards/card_gender/card_gender.dart';
import '../constants.dart';

enum GenderEnum { male, female }

class InputPage extends StatefulWidget {
  const InputPage({Key? key}) : super(key: key);

  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  GenderEnum? selectedGenderCard;
  int selectedHeight = 160;
  int selectedWeight = 76;
  int selectedAge = 30;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BMI CALCULATOR'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: BaseCardTemplate(
                    onPressed: () {
                      setState(() {
                        selectedGenderCard = GenderEnum.male;
                      });
                    },
                    color: selectedGenderCard == GenderEnum.male
                        ? kActiveCardColor
                        : kInactiveCardColor,
                    cardChild: const GenderCardTemplate(
                      icon: FontAwesomeIcons.mars,
                      genderLabel: "MALE",
                    ),
                  ),
                ),
                Expanded(
                  child: BaseCardTemplate(
                    onPressed: () {
                      setState(() {
                        selectedGenderCard = GenderEnum.female;
                      });
                    },
                    color: selectedGenderCard == GenderEnum.female
                        ? kActiveCardColor
                        : kInactiveCardColor,
                    cardChild: const GenderCardTemplate(
                      icon: FontAwesomeIcons.venus,
                      genderLabel: "FEMALE",
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: BaseCardTemplate(
              color: kActiveCardColor,
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text('HEIGHT', style: kLabelTextStyle),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: <Widget>[
                      Text(selectedHeight.toString(), style: kLeadTextStyle),
                      const Text('cm', style: kLabelTextStyle),
                    ],
                  ),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                        activeTrackColor: kBottomContainerColor,
                        inactiveTrackColor: kScaffoldBackgroundColor,
                        thumbColor: kBottomContainerColor,
                        thumbShape: const RoundSliderThumbShape(
                          enabledThumbRadius: 15.0,
                        ),
                        overlayShape: const RoundSliderOverlayShape(
                          overlayRadius: 30.0,
                        ),
                        overlayColor: kBottomContainerColor.withOpacity(0.2)),
                    child: Slider(
                      value: selectedHeight.toDouble(),
                      min: kHeightSliderMinValue,
                      max: kHeightSliderMaxValue,
                      onChanged: (double x) {
                        setState(() {
                          selectedHeight = x.round();
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: BaseCardTemplate(
                    color: kActiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text(
                          'WEIGHT',
                          style: kLabelTextStyle,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Text(
                              selectedWeight.toString(),
                              style: kLeadTextStyle,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RoundIconButton(
                              icon: FontAwesomeIcons.minus,
                              onPressed: () {
                                setState(() {
                                  selectedWeight--;
                                });
                              },
                            ),
                            RoundIconButton(
                              icon: FontAwesomeIcons.plus,
                              onPressed: () {
                                setState(() {
                                  selectedWeight++;
                                });
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: BaseCardTemplate(
                    color: kActiveCardColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text(
                          'AGE',
                          style: kLabelTextStyle,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.alphabetic,
                          children: <Widget>[
                            Text(
                              selectedAge.toString(),
                              style: kLeadTextStyle,
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RoundIconButton(
                              icon: FontAwesomeIcons.minus,
                              onPressed: () {
                                setState(() {
                                  selectedAge--;
                                });
                              },
                            ),
                            RoundIconButton(
                              icon: FontAwesomeIcons.plus,
                              onPressed: () {
                                setState(() {
                                  selectedAge++;
                                });
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          BottomButton(
            buttonText: "CALCULATE",
            handleOnTap: () {
              BmiCalculatorService service = BmiCalculatorService(
                heightInCm: selectedHeight,
                weightInKg: selectedWeight,
              );
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ResultPage(
                            bmiResult: service.getBmiAsString(),
                            bmiResultText: service.getBmiResultText(),
                            bmiResultInterpretation:
                                service.getBmiResultInterpretation(),
                          )));
            },
          ),
        ],
      ),
    );
  }
}
