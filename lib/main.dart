import 'package:flutter/material.dart';

import 'constants.dart';
import 'pages/input_page.dart';

void main() => runApp(BMICalculator());

class BMICalculator extends StatelessWidget {
  const BMICalculator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // https://docs.flutter.dev/cookbook/design/themes
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: kScaffoldBackgroundColor,
        colorScheme: const ColorScheme.light().copyWith(
          primary: const Color(0xFF0A0E21),
        ),
      ),
      home: const InputPage(),
    );
  }
}
