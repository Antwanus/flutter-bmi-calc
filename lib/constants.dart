import 'package:flutter/material.dart';

//
// INPUT PAGE (HOME)
//
const kLabelTextStyle = TextStyle(
  fontSize: 18,
  color: Color(0xFF8D8E98),
);
const kLeadTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.w900,
);

const kScaffoldBackgroundColor = Color(0xFF0A0E21);

// GENDER-ROW
const kActiveCardColor = Color(0xFF1D1E33);
const kInactiveCardColor = Color(0xFF111328);

// HEIGHT-ROW/COL
const double kHeightSliderMinValue = 130.0;
const double kHeightSliderMaxValue = 220.0;

// BOTTOM-ROW
const kBottomContainerHeight = 80.0;
const kBottomContainerColor = Color(0xFFEB1555);

//
// RESULT PAGE
//
const kLargeButtonStyle =
    TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold);
const kTitleTextStyle = TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold);
const kResultTextStyle = TextStyle(
  color: Color(0xFF24D876),
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);
const kBMIResultTextStyle = TextStyle(
  fontSize: 100.0,
  fontWeight: FontWeight.bold,
);
const kResultBodyTextStyle = TextStyle(fontSize: 30.0);
